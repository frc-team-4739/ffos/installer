# FFOS, an install script to provide a powerful yet simple Arch install
# Copyright (C) 2020  Evan La Fontaine (FRC Team 4739, CTRL F5)

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


wget -q --spider http://google.com
if [ $? -eq 0 ]
then
    pacman --noconfirm -Sy ff-install
else
    touch /root/offline
fi

echo "
[options]
HoldPkg             = pacman glibc
Architecture        = auto
CheckSpace
SigLevel            = Required DatabaseOptional
LocalFileSigLevel   = Optional

[offline]
SigLevel            = Optional TrustAll
Server              = file:///root/pkg-offline
" > /etc/pacman.conf
ff-install
