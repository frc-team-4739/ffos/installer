# FFOS, an install script to provide a powerful yet simple Arch install
# Copyright (C) 2020  Evan La Fontaine (FRC Team 4739, CTRL F5)

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


sed -i "s/__BUILD__/$CI_PIPELINE_IID/" variables.sh
sed -i "s@__SERVER__@$PRODUCTION_IP@g" variables.sh
sed -i "s@__SERVER_TESTING__@$TESTING_IP@g" variables.sh
. variables.sh

cp -r /usr/share/archiso/configs/releng/ ./live-usb
chmod 777 build/customize.sh
build/customize.sh

echo "
[archlinuxcn]
Server = https://repo.archlinuxcn.org/\$arch
" >> live-usb/pacman.conf

# https://gitlab.com/gitlab-com/support-forum/issues/4280
mknod /dev/loop0 -m0660 b 7 0
mknod /dev/loop1 -m0660 b 7 1
mknod /dev/loop2 -m0660 b 7 2
mknod /dev/loop3 -m0660 b 7 3
mknod /dev/loop4 -m0660 b 7 4
mknod /dev/loop5 -m0660 b 7 5
mknod /dev/loop6 -m0660 b 7 6
mknod /dev/loop7 -m0660 b 7 7
mknod /dev/loop8 -m0660 b 7 8
mknod /dev/loop9 -m0660 b 7 9

mkarchiso -v                    \
    -L FFOS_$VERSION_TAG        \
    -P 'FRC Team 4739, Ctrl F5' \
    -A 'FFOS Installer'         \
    -o ./                       \
    -p kexec-tools              \
    ./live-usb

mv *.iso ffos-$VERSION_NO.iso
