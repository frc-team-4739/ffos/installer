# FFOS, an install script to provide a powerful yet simple Arch install
# Copyright (C) 2020  Evan La Fontaine (FRC Team 4739, CTRL F5)

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

sed -i "s/__BUILD__/$CI_PIPELINE_IID/" variables.sh
sed -i "s@__SERVER__@$PRODUCTION_IP@g" variables.sh
sed -i "s@__SERVER_TESTING__@$TESTING_IP@g" variables.sh
. variables.sh

[ $verMode==r ] && T=''  || T='#'
[ $verMode==r ] && R='#' || R=''

echo "
${T}[ffps-testing]
${T}SigLevel = Optional TrustAll
${T}Server = http://$PACKAGE_SERVER_TESTING/packages

${R}[ffps]
${R}SigLevel = Optional TrustAll
${R}Server = http://$PACKAGE_SERVER/packages
[archlinuxcn]
Server = https://repo.archlinuxcn.org/\$arch
" >> /etc/pacman.conf
pacman --noconfirm -Syu grep file archiso pacman-contrib
