#!/bin/bash

# FFOS, an install script to provide a powerful yet simple Arch install
# Copyright (C) 2020  Evan La Fontaine (FRC Team 4739, CTRL F5)

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


. variables.sh
cd live-usb

mv ../bootstrap.sh airootfs/root/
chmod 777 airootfs/root/bootstrap.sh
REPLACEMENT="script=/root/bootstrap.sh"
# https://unix.stackexchange.com/questions/368697/automatically-running-an-installation-cli-script-in-terminal-on-startup
# https://stackoverflow.com/questions/9366816/sed-fails-with-unknown-option-to-s-error
sed -i "s@^options@& $@$REPLACEMENT@" \
efiboot/loader/entries/archiso-x86_64-linux.conf
sed -i "s@^APPEND@& $@$REPLACEMENT@" \
syslinux/archiso_sys.cfg

sed -i "s/timeout .*/timeout 0/" \
efiboot/loader/loader.conf

echo 'ff-install
reflector
wget' >> packages.x86_64


# used for commenting out the correct repo
[ $verMode==r ] && T=''  || T='#'
[ $verMode==r ] && R='#' || R=''


read -d '' package_info << EOF

${T}[ffps-testing]
${T}SigLevel = Optional TrustAll
${T}Server = http://$PACKAGE_SERVER_TESTING/packages

${R}[ffps]
${R}SigLevel = Optional TrustAll
${R}Server = http://$PACKAGE_SERVER/packages

[multilib]
Include = /etc/pacman.d/mirrorlist
EOF
echo "$package_info" >> pacman.conf
echo "$package_info" >> /etc/pacman.conf

pacman -Sy
packages=$(pactree -su ff-all | grep -v "nvidia" | grep -v 'xorg-luit' | tr '\n' ' ')
mkdir airootfs/root/pkg-offline
pacman -Sw $packages luit --cachedir airootfs/root/pkg-offline --noconfirm
repo-add airootfs/root/pkg-offline/offline.db.tar.gz airootfs/root/pkg-offline/*.pkg.tar.*

echo "
echo \"$package_info\" >> /etc/pacman.conf
" >> airootfs/root/customize_airootfs.sh

cd ..
