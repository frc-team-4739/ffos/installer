sed -i "s/__BUILD__/$CI_PIPELINE_IID/" variables.sh
mv variables.sh install

cd install
SOURCES=()
for FILE in ./*.py; do SOURCES+=("\"$FILE\""); done
STR_SOURCES=${SOURCES[@]}
# https://stackoverflow.com/questions/9366816/sed-fails-with-unknown-option-to-s-error
sed -i "s@__SOURCES__@$STR_SOURCES@g" PKGBUILD

useradd builduser
passwd -d builduser
su -c "makepkg -g >> PKGBUILD" builduser
su -c "PKGEXT=.pkg.tar.lz4 makepkg --noconfirm" builduser
mv ff-install-*.pkg.tar.lz4 ../
