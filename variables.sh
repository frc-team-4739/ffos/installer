# FFOS, an install script to provide a powerful yet simple Arch install
# Copyright (C) 2020  Evan La Fontaine (FRC Team 4739, CTRL F5)

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


verPre='b'
verLngPre='B'
verMajor=2
verMinor=1
verCode='Kellyville'
# 't' for testing or 'r' for release
verMode=__MODE__
verLngMode=__MODE_LNG__
verBuild=__BUILD__

export VERSION_PLAIN=$verMajor.$verMinor.$verBuild
export VERSION_NO=$verPre$verMode$verMajor.$verMinor.$verBuild
export VERSION_TAG=$verLngPre$verLngMode$verMajor$verMinor\_$verBuild
export VERSION_TITLE="$verLngPre$verLngMode $verMajor.$verMinor"


export PACKAGE_SERVER='__SERVER__'
export PACKAGE_SERVER_TESTING='__SERVER_TESTING__'
