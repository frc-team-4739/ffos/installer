#!/usr/bin/env python3

# FFOS, an install script to provide a powerful yet simple Arch install
# Copyright (C) 2020  Evan La Fontaine (FRC Team 4739, CTRL F5)

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


from dialog import Dialog
from utils import wrapped
from do_install import Configuration
from utils import Canceled
from parted import Device


def DiskString(dev: Device):
    """
    """
    
    return "%s (%s)" % (dev.model, dev.path[5:])

def confirm_configuration(dialog: Dialog, config: Configuration):
    swap = config.disk.swap
    if swap is None:
        swap = 0
    else:
        swap = swap.getLength('GiB')
        
    if config.disk.format_disks == None:
        format_disks = "None"
    else:
        format_disks = ", ".join(map(DiskString, config.disk.format_disks))
        
    if not isinstance(config.disk.efi, str):
        disk_efi = DiskString(config.disk.efi.device)
    else:
        disk_efi = config.disk.efi
    
    
    code = dialog.yesno(
        "Configuration complete please confirm your options before "
        "proceeding as they are difficult or impossible to modify "
        "later. This is the last point in which it is safe to cancel "
        "the installation (No changes have been made yet)."
        "\n\n"
        "Formatted Drives: %s\n"
        "EFI Location: %s\n"
        "Install Location: %s\n"
        "Data Size: %d GiB\n"
        "Swap Size: %d GiB\n"
        "\n"
        "Has User: %s\n"
        "Username: %s\n"
        "Packages: %s\n"
        % (
            format_disks,
            disk_efi,
            DiskString(config.disk.data.device),
            config.disk.data.getLength('GiB'),
            swap,
            
            str(config.credentials.has_user),
            config.credentials.name,
            ", ".join(config.credentials.packages)
        ),
        
        extra_button=True,
        extra_label="Restart",
        cancel_label="Cancel",

        backtitle="FFOS Installer",
        title="Confirm Options",

        width=80,
        height=24
    )
        
    if code == Dialog.CANCEL:
        raise Canceled()
    return code == Dialog.OK
