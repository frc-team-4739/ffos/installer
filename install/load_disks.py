#!/usr/bin/env python3

# FFOS, an install script to provide a powerful yet simple Arch install
# Copyright (C) 2020  Evan La Fontaine (FRC Team 4739, CTRL F5)

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


from typing import List, Dict, Tuple, Union
from utils import wrapped
from dialog import Dialog
from dataclasses import dataclass, field
from psutil import virtual_memory
from math import ceil
from parted import newDisk, getAllDevices, Device, Geometry
import parted


@dataclass
class Candidate:
    """
    A struct which represents where FFOS could be installed
    """

    menu_entry: str = ""
    device: Device = None
    start: int = 0
    length: int = 0

@dataclass
class LoadResult:
    """
    Output from this stage of preparation which is infomation
    about the potential install location
    """

    candidates: Dict[str, Candidate] = field(default_factory=dict)
    format_disks: List[Device] = field(default_factory=list)
    efi: Union[str, Geometry] = None
    swap_size: int = 0
    disk_size: int = 0

@dataclass
class ScanResult:
    """
    The result from an intial scan of the disks with no user input
    to identify the efi partition if it exists along with any
    potential install location
    """

    unformated: List[Device] = field(default_factory=list)
    candidates: Dict[str, Candidate] = field(default_factory=dict)
    efi_partition: Union[str, Geometry] = None

def RequiredSize(dialog: Dialog) -> (int, int):
    """
    Calculates and queries the user for the the amount of disk space 
    required both for data and swap.
    """

    selection = wrapped(dialog.menu(
        "Select the amount of swap space desired for this installation. "
        "In Windows NT nomenclature this is a pagefile. Swap acts as "
        "space on your hard drive assigned to be used as additional RAM "
        "at a lower speed."
        "\n\n"
        "When the system runs out of real RAM it will start using swap, "
        "but when there is no swap left or if swap is disabled "
        "applications will be randomly terminated. Applications will run "
        "significantly slower if they use swap (especially if a HDD is "
        "used instead of an SSD).",
        height=24,
        width=80,
        backtitle="FFOS Installer - Drives & Partitions",
        title="Swap Space",
        choices=[
            ("Disabled", "Only recommended for VMs"),
            ("Partial", "Recommended for desktops"),
            ("Enabled", "Allows hibenation, recommended for laptops")
        ]
    ))

    ram = virtual_memory().total // 2**30
    swap = 0
    if selection == "Partial":
        swap =  int(ceil(ram**0.5))
    if selection == "Enabled":
        swap = ram + int(ceil(ram**0.5))
    return (12 + swap, swap)

def ScanDisks(required_length: int) -> ScanResult:
    """
    Scans the attached drives for free spaces as well as for the EFI 
    partiton
    """

    result = ScanResult()
    for dev in getAllDevices():
        try:
            disk = newDisk(dev)

            for partition in disk.partitions:
                if partition.name == "EFI system partition":
                    result.efi_partition = partition.path

            best_start = 0
            best_length = 0
            for free in disk.getFreeSpaceRegions():
                if free.length > best_length:
                    best_length = free.length
                    best_start = free.start
            if best_length < required_length:
                continue
            # Remove dev/ prefix from name
            result.candidates[
                "%s (%s)" % (dev.model, dev.path[5:])
            ] = Candidate(
                "Size: %dGB Free: %dGB" % 
                    (dev.length // 2**21, best_length // 2**21),
                dev,
                best_start,
                best_length
            )
        except parted._ped.DiskException:
            result.unformated.append(dev)
    return result

def SelectUnformated(dialog: Dialog, unformated: List[Device]):
    """
    Prompts the user to select disks which they would like to
    format
    """

    if unformated != []:
        choices_dict = {}
        for uf in unformated:
            choices_dict["%s (%s)" % (uf.model, uf.path[5:])] = uf
        return list(map(
            lambda x: choices_dict[x],
            wrapped(dialog.checklist(
                "Select drives to format. This should only have to occour if "
                "a new hard drive is used (including virtual drives).",
                choices=map(
                    lambda x: (x, "", False),
                    choices_dict.keys()
                ),
                backtitle="FFOS Installer - Drives & Partitions",
                title="Drive formating"
            ))
        ))

def AddFormatCandidates(
    candidates: Dict[str, Candidate],
    unformated: List[Device],
    required_disk: int
):
    """
    Add candidate spaces for unformated drives
    """

    if unformated is not None:
        for dev in unformated:
            size = dev.getLength()
            if size > required_disk:
                candidates[
                    "%s (%s)" % (dev.model, dev.path[5:])
                ] = Candidate(
                    "Size: %dGB Free: %dGB"
                        % (size // 2**21, size // 2**21),
                    dev,
                    2048,
                    size
                )

    if len(candidates) == 0:
        raise Exception(
            "No free space found on any connected disks. Ensure "
            "that space is left unpartitioned."
        )


def LoadDisks(dialog: Dialog) -> LoadResult:
    """
    Gets the arguments used later to partition disks
    """
    
    result = LoadResult()
    result.disk_size, result.swap_size = RequiredSize(dialog)

    scan = ScanDisks(result.disk_size * 2**21)
    result.efi = scan.efi_partition
    result.format_disks = SelectUnformated(dialog, scan.unformated)
    result.candidates = scan.candidates
    AddFormatCandidates(result.candidates, result.format_disks, result.disk_size * 2**21)
    
    return result
