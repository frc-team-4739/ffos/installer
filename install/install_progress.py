#! /usr/bin/env python3

# FFOS, an install script to provide a powerful yet simple Arch install
# Copyright (C) 2020  Evan La Fontaine (FRC Team 4739, CTRL F5)

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


from threading import Thread, Lock, Event
from subprocess import check_output
from dialog import Dialog
from time import sleep
from os import remove
from os.path import exists


bar_progress = 0
bar_next = 0
bar_title = ""
bar_lock = Lock()
def UpdateBar(val: int, nxt: int, msg: str):
    """
    Controls the heading of the progress bar along with the estimated
    completion percentage
    """

    global bar_progress
    global bar_next
    global bar_title

    bar_lock.acquire()
    bar_progress = val
    bar_next = nxt
    bar_title = msg
    bar_lock.release()

bar_running = Event()
bar_complete = Event()
def progressBar(dialog: Dialog):
    """
    Another thread to operate the progress bar also reading from the
    output of bash functions
    """

    global bar_running
    global bar_complete
    global bar_progress
    global bar_next
    global bar_title
    bar_running.set()

    dialog.gauge_start(
        backtitle="FFOS Installer - Installation",
        title="Installing...", 
        height=12, 
        width=80
    )

    path = "/root/progress"
    pv = 0
    while bar_running.is_set():
        if not exists(path):
            path = "/root/progress"
        try:
            msg = str(check_output(["tail", "-1", path]))[2:-1]
        except:
            pass
        if len(msg) > 72:
            msg = msg[:72]
        if msg == "":
            continue
        if "chroot install" in msg:
            path = "/mnt/root/progress"
        bar_lock.acquire()
        pv = max(bar_progress, pv) 
        pv += (bar_next - pv) * 0.0007
        dialog.gauge_update(
            int(pv),
            "%s:\n\n%s" % (bar_title, msg), 
            True
        )
        bar_lock.release()
        sleep(1/60)
    dialog.gauge_stop()
    bar_running.clear()
    bar_complete.set()
