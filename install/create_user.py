#!/usr/bin/env python3

# FFOS, an install script to provide a powerful yet simple Arch install
# Copyright (C) 2020  Evan La Fontaine (FRC Team 4739, CTRL F5)

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


from chroot_utils import Credentials, prog, run
from pickle import load
from os import system, mkdir, remove
from shutil import rmtree


def create_user():
    """
    A script which will do final configuration inside the root of the
    new install (through arch-chroot)
    """

    with open("/root/installer/credentials", "rb") as cf:
        credentials: Credentials = load(cf)
    
    run("mkinitcpio -p linux")

    prog("creating user")
    run("echo 'root:%s' | chpasswd" % credentials.password)
    if credentials.name != "":
        run("useradd -m -g users -s /bin/bash %s" % credentials.name)
        run("echo '%s:%s' | chpasswd" % (credentials.name, credentials.password))

        prog("granting permissions")
        with open("/etc/sudoers", "w") as sf:
            sf.write("root ALL=(ALL) ALL\n")
            sf.write("%s ALL=(ALL) ALL\n" % credentials.name)
            
    prog("setting host name")
    with open("/etc/hostname", "w") as hf:
        hf.write("ffos-%s" % credentials.name)
        
    prog("installing bootloader")
    run("os-prober")
    run("grub-install --target=x86_64-efi --bootloader-id=grub_uefi --efi-directory=/boot --recheck")
    run("grub-mkconfig -o /boot/grub/grub.cfg")
        
    rmtree("/root/installer")

if __name__ == "__main__":
    create_user()
