#!/usr/bin/env python3

# FFOS, an install script to provide a powerful yet simple Arch install
# Copyright (C) 2020  Evan La Fontaine (FRC Team 4739, CTRL F5)

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


from chroot_utils import Credentials, run
from dataclasses import dataclass
from dialog import Dialog
from utils import wrapped, Canceled
from partition import PartitionDisks
from re import compile, match


def GetHasUser(dialog: Dialog) -> bool:
    """
    Asks the user wheather a user account should be created
    """
    
    result = dialog.yesno(
        "For a normal desktop install a user account "
        "should be created, but for a server a user "
        "account should not be created (read the "
        "documentation on servers). If you do this on a "
        "workstation install you will not be able to login.",

        ok_label="Yes",
        cancel_label="Cancel",

        extra_button=True,
        extra_label="No",

        backtitle="FFOS Installer - Credentials",
        title="Create User Account",

        width=80,
        height=12
    )
    
    if result == Dialog.CANCEL:
        raise Canceled()
    if result == Dialog.OK:
        return True
    else:
        return False

def GetUsername(dialog: Dialog) -> str:
    """
    Asks the user to enter a valid username
    """
    
    valid_username = compile("^[a-zA-Z][a-zA-Z0-9-]*[a-zA-Z0-9]$")
    while True:
        username = wrapped(dialog.inputbox(
            "Enter username/systemname",
            height=12,
            width=80,
            init="",
            backtitle="FFOS Installer - Credentials", 
            title="Username"
        ))
        
        if valid_username.match(username):
            return username
        
        dialog.msgbox(
            "A name must be at least two alphanumeric characters "
            "(including hyphens). It cannot start or end with a hypen "
            "and cannot start with a number.",
            height=8,
            width=80,
            backtitle="FFOS Installer - Credentials", 
            title="Username"
        )
        

def GetPassword(dialog: Dialog) -> str:
    """
    Asks the user to enter a password and confirms it
    """

    while True:
        password = wrapped(dialog.passwordbox(
            "Enter password",
            height=8,
            width=80,
            init="",
            insecure=True,
            backtitle="FFOS Installer - Credentials", 
            title="Password"
        ))

        if wrapped(dialog.passwordbox(
            "Confirm password",
            height=8,
            width=80,
            init="",
            insecure=True,
            backtitle="FFOS Installer - Credentials", 
            title="Password"
        )) == password:
            return password

        dialog.msgbox(
            "Passwords do not match",
            height=8,
            width=80,
            backtitle="FFOS Installer - Credentials", 
            title="Password"
        )
        

def GetCredentials(dialog: Dialog) -> Credentials:
    """
    Gets the credentials from the user which includes the name,
    password and packages
    """

    result = Credentials()
    result.has_user = GetHasUser(dialog)
    result.packages = wrapped(dialog.checklist(
        "Select the packages to install on this machine which "
        "will determine the default applications to install.",
        height=16,
        width=80,
        backtitle="FFOS Installer - Credentials", 
        title="Role",
        choices=[
            ("server"       , "", False),
            ("workstation"  , "", True ),
            ("extra"        , "", True ),
            ("programmer"   , "", False),
            ("designer"     , "", False),
            ("administrator", "", False),
        ]
    ))
    
    result.name = GetUsername(dialog)
    result.password = GetPassword(dialog)

    return result
