#!/usr/bin/env python3

# FFOS, an install script to provide a powerful yet simple Arch install
# Copyright (C) 2020  Evan La Fontaine (FRC Team 4739, CTRL F5)

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import subprocess
from typing import List
from tempfile import mkstemp
from os import write, read, close, system


def clear_screen():
    """
    Prints an escape character to the terminal to clear it
    """

    print(chr(27) + "[2J")

class Error(Exception):
    """
    Base class for other exceptions
    """

    pass

class Canceled(Error):
    """
    Signal for the installation being cancelled
    """

    pass

def wrapped(result):
    """
    Wraps dialog calls to cancel the installation when
    the cancel button is pressed
    """
    
    if type(result) == str:
        if result == "ok":
            return
        raise Canceled()
    if result[0] == "ok":
        return result[1]
    raise Canceled()
