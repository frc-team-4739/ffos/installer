#!/usr/bin/env python3

# FFOS, an install script to provide a powerful yet simple Arch install
# Copyright (C) 2020  Evan La Fontaine (FRC Team 4739, CTRL F5)

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


from dialog import Dialog

import utils
import os


def Intro(dialog: Dialog):
    """
    Introduce the users to FFOS, the install and it's liscense
    """
    
    while True:
        code = dialog.yesno(
            "Before you begin please ensure that the required setup has been "
            "performed on your main operating system before commencing "
            "installation if applicable. It is currently safe to restart your "
            "machine if you have not done this"
            "\n\n"
            "Follow the instructions on screen and consult the guide if you are "
            "not sure. If you are still unable to fix the issues do NOT force "
            "things and instead get in contact with a member of the software "
            "subteam."
            "\n\n"
            "It is safe to remove power or abort the installation until the "
            "installation phase is reached (see top left corner) as no changes "
            "are actually made to your system until then.",
            
            extra_button=True,
            help_button=True,
            ok_label="Continue",
            cancel_label="Exit",
            extra_label="Reboot",
            help_label="Liscense",

            backtitle="FFOS Installer - Introduction",
            title="Welcome",

            width=80,
            height=16
        )

        if code == Dialog.CANCEL:
            utils.clear_screen()
            exit()
        elif code == Dialog.EXTRA:
            os.system("reboot")
        elif code == Dialog.HELP:
            dialog.textbox(
                "/usr/share/licenses/common/GPL3/license.txt",
                backtitle="FFOS Installer - Introduction",
                title="Liscense",
                width=80,
                height=36
            )
        else:
            return
