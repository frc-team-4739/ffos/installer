#! /usr/bin/env python3

# FFOS, an install script to provide a powerful yet simple Arch install
# Copyright (C) 2020  Evan La Fontaine (FRC Team 4739, CTRL F5)

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


from select_disk import DiskConfig
from partition import PartitionDisks
from install_progress import progressBar, bar_complete, bar_running, UpdateBar

from chroot_utils import run, Credentials
from dialog import Dialog
from pickle import dump
from threading import Thread
from time import sleep
from shutil import copyfile
from os import chmod, mkdir, system, remove
from dataclasses import dataclass
from pathlib import Path


@dataclass
class Configuration:
    """
    A struct which contans the installation infomation
    """

    disk: DiskConfig = None
    credentials: Credentials = None

def install(dialog: Dialog, config: Configuration):
    """
    Based on a configuration object install FFOS, only prompting the
    user if there are errors
    """

    global bar_running
    global bar_complete
    dialog.gauge_start(
        backtitle="FFOS Installer - Installation",
        title="Installing...", 
        height=12, 
        width=80
    )
    progress = Thread(target=progressBar, args=(dialog, ))
    progress.start()
    bar_running.wait()

    try:
        UpdateBar(0, 5, "Partitioning disks")
        PartitionDisks(config.disk)
        UpdateBar(6, 87, "Installing packages")
        run("pacstrap /mnt ff-base ff-%s" % " ff-".join(config.credentials.packages))

        UpdateBar(88, 90, "Preparing for chroot")
        mkdir("/mnt/root/installer")
        copyfile("/bin/ff-install.d/create_user.py", "/mnt/root/installer/create_user.py")
        chmod("/mnt/root/installer/create_user.py", 777)
        copyfile("/bin/ff-install.d/chroot_utils.py", "/mnt/root/installer/chroot_utils.py")
        with open("/mnt/root/installer/credentials", "wb") as df:
            dump(config.credentials, df)
        Path("/mnt/root/progress").touch()
        with open("/root/progress", "a") as pf:
            pf.write("chroot install\n")
        UpdateBar(91, 96, "Adding users")
        run("arch-chroot /mnt /root/installer/create_user.py")
        remove("/mnt/root/progress")
        UpdateBar(97, 98, "Generating fstab")
        system("genfstab /mnt >> /mnt/etc/fstab")
        UpdateBar(99, 100, "Cleaning up")
        run("umount /mnt/boot")
        run("umount /mnt")
    except:
        bar_running.clear()
        bar_complete.wait()
        raise
    bar_running.clear()
    bar_complete.wait()

    pass
