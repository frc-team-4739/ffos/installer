#!/usr/bin/env python3

# FFOS, an install script to provide a powerful yet simple Arch install
# Copyright (C) 2020  Evan La Fontaine (FRC Team 4739, CTRL F5)

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


from load_disks import LoadResult, Candidate
from utils import wrapped
from typing import Dict, Union, List
from parted import Geometry, Device
from dataclasses import dataclass, field
from dialog import Dialog


@dataclass
class DiskConfig:
    """
    Struct containing infomation on how to format disks
    """

    format_disks: List[Device] = field(default_factory=list)
    efi: Union[str, Geometry] = None
    swap: Geometry = None
    data: Geometry = None


def SelectInstallLocation(
        dialog: Dialog, 
        candidates: Dict[str, Candidate]
    ) -> Candidate:
    """
    Prompts the user to select where to install the OS
    """

    space = list(candidates.keys())[0]
    if len(candidates) == 1:
        wrapped(dialog.msgbox(
            "One empty space found on disk %s (%s). This will "
            "be used automatically." % (space, candidates[space].menu_entry),
            height=12,
            width=80,
            extra_button=True,
            extra_label="Cancel",
            backtitle="FFOS Installer - Drives & Partitions",
            title="Space Selection"
        ))
    else:
        space = wrapped(dialog.menu(
            "Select the position to install FFOS to.",
            height=12,
            width=80,
            choices=map(
                lambda x: (x[0], x[1].menu_entry),
                zip(candidates.keys(), candidates.values())
            ),
            backtitle="FFOS Installer - Drives & Partitions",
            title="Space Selection"
        ))
    return candidates[space]
    
def CreateGeometries(
        dialog: Dialog, 
        location: Candidate, 
        efi: Union[str, Geometry],
        swap_size: int,
        required_disk: int
    ) -> (Union[str, Geometry], Geometry, Geometry):
    """
    Calculates the layout for where the new partition will be
    placed
    """

    if location.start < 2048:
        offset = 2048 - location.start
    else:
        offset = 0
    
    if efi == None:
        wrapped(dialog.msgbox(
            "No EFI partition found either allow one to be "
            "automatically or cancel the installation. This "
            "should only occour on a BIOS system (not supported)"
            "or on a completely fresh system/VM.",
            height=12,
            width=80,
            extra_button=True,
            extra_label="Cancel",
            backtitle="FFOS Installer - Drives & Partitions",
            title="EFI Partition"
        ))
        length = 384 * 2**11
        efi = Geometry(location.device, offset + location.start, length)
        offset += length

    swap = None
    if swap_size != 0:
        length = swap_size * 2**21
        swap = Geometry(location.device, location.start + offset, length)
        offset += length

    max_data = (location.length // 2**21)  - swap_size
    data_size = wrapped(dialog.rangebox(
        "Select the amount of space dedicated to data (not swap).",
        height=12,
        width=80,
        min=required_disk - swap_size,
        max=max_data,
        init=max_data,
        backtitle="FFOS Installer - Drives & Partitions",
        title="Data Space"
    ))
    length = data_size * 2**21
    if data_size == max_data:
        length = location.length - offset
    data = Geometry(location.device, location.start + offset, length)

    return efi, swap, data

def SelectDisk(dialog: Dialog, loaded: LoadResult) -> DiskConfig:
    """
    Selects the install location from the user and calculates the exact
    location
    """

    config = DiskConfig(loaded.format_disks, loaded.efi)
    config.efi, config.swap, config.data = CreateGeometries(
        dialog, 
        SelectInstallLocation(dialog, loaded.candidates),
        loaded.efi,
        loaded.swap_size,
        loaded.disk_size
    )
    return config
