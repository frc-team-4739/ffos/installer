#! /usr/bin/env python3

# FFOS, an install script to provide a powerful yet simple Arch install
# Copyright (C) 2020  Evan La Fontaine (FRC Team 4739, CTRL F5)

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


from intro import Intro
from load_disks import LoadDisks
from select_disk import DiskConfig, SelectDisk
from get_credentials import GetCredentials, Credentials
from do_install import install, Configuration
from confirmation import confirm_configuration

from utils import clear_screen, Canceled
from traceback import format_exc
from os import system
from dialog import Dialog
from dataclasses import dataclass


def configure(dialog: Dialog) -> Configuration:
    """
    Prompt from the user with questions on how to configure the
    system in a series of menus
    """

    config = Configuration()
    config.disk = SelectDisk(dialog, LoadDisks(dialog))
    config.credentials = GetCredentials(dialog)
    return config

def process_failure(dialog: Dialog, title_name: str, code: str):
    """
    Processes the very similar cases for the two types of installation
    failure (cancel or an error) to reduce code reuse
    """

    if code == Dialog.OK:
        system("reboot")
    elif code == Dialog.CANCEL:
        dialog.msgbox(
            format_exc(), 
            backtitle="FFOS Installer", 
            title=title_name,
            width=80,
            height=36
        )
    else:
        clear_screen()
        exit()


def installer_main():
    """
    Install and configure an FFOS installation, prompting for user
    input as required
    """

    dialog = Dialog()
    Intro(dialog)
    try:
        while True:
            config = configure(dialog)
            if confirm_configuration(dialog, config):
                break
        
        install(dialog, config)
    except Canceled:
        while True:
            process_failure(
                dialog,
                "Installation cancelled",
                dialog.msgbox(
                    "The installation has been cancelled or interrupted. The "
                    "system can be rebooted or the command line of the live "
                    "USB can be used to make changes before running the "
                    "installer again."
                    "\n\n"
                    "NOTE: If you elected to do this during the installation "
                    "phase changes may have already been made to the system "
                    "which could cause the system to fail to boot.",

                    ok_label="Reboot",
                    extra_button=True,
                    extra_label="Exit",

                    backtitle="FFOS Installer",
                    title="Installation cancelled",

                    width=80,
                    height=12
                )
            )
    except:
        with open("/root/log.txt", "w") as log:
            log.write(format_exc())
        while True:
            process_failure(
                dialog,
                "Installation failed",
                dialog.yesno(
                    "The installer encountered an unhandled fatal error and "
                    "could not continue. The system can be rebooted or the "
                    "command line of the live USB can be used to try and fix "
                    "the issue. The error can also be viewed."
                    "\n\n"
                    "NOTE: If this failure occoured in the instllation phase "
                    "and depending on the specific error your system may fail "
                    "to boot even into the orignal OS. It is strongly advised "
                    "that you take a look at the error and either resolve it "
                    "or seek technical support.",

                    ok_label="Reboot",
                    cancel_label="View Error",

                    extra_button=True,
                    extra_label="Exit",

                    backtitle="FFOS Installer",
                    title="Installation failed",

                    width=80,
                    height=12
                )
            )
    else:
        code = dialog.pause(
            "You can either reboot and use FFOS or exit this installer "
            "to the command line in the live USB to do additional "
            "configuration. If no option is selected then the system "
            "will automatical reboot in 20 seconds.", 
            seconds=20,
            width=80,

            ok_label="Reboot",
            cancel_label="Exit",

            backtitle="FFOS Installer - Installation",
            title="Installation complete"
        )

        if code == Dialog.OK:
            system("reboot")
        else:
            clear_screen()
            exit()

if __name__ == "__main__":
    installer_main()
