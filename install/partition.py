#!/usr/bin/env python3

# FFOS, an install script to provide a powerful yet simple Arch install
# Copyright (C) 2020  Evan La Fontaine (FRC Team 4739, CTRL F5)

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


from select_disk import DiskConfig
from dataclasses import dataclass
from utils import wrapped
from os import system, mkdir
from chroot_utils import run, prog
from parted import Partition, Disk, Geometry, freshDisk, FileSystem, \
    PARTITION_SWAP, PARTITION_NORMAL, PARTITION_BOOT, partitionFlag, \
    Constraint

import parted


def PartitionDisks(config: DiskConfig):
    """
    Partition and mount disks
    """

    if config.format_disks is not None:
        for unformated in config.format_disks:
            prog("formatting %s as gpt" % unformated.path)
            freshDisk(unformated, "gpt").commit()
    disk_main = Disk(config.data.device)
    cnst_main = Constraint(device=config.data.device)

    if type(config.efi) == Geometry:
        prog("creating efi partition on %s" % config.efi.device.path)
        p = Partition(
            disk=disk_main,
            type=PARTITION_NORMAL,
            geometry=config.efi
        )
        p.setFlag(18) # esp
        p.setFlag( 1) # boot
        p.setFlag( 4) # hidden
        disk_main.addPartition(p, cnst_main)
        disk_main.commit()
        run("mkfs.fat -F32 %s" % p.path)
        config.efi = p.path

    if config.swap != None:
        prog("creating swap partition on %s" % config.swap.device.path)
        p = Partition(
            disk=disk_main,
            type=PARTITION_NORMAL,
            geometry=config.swap
        )
        disk_main.addPartition(p, cnst_main)
        disk_main.commit()
        run("mkswap %s" % p.path)
        run("swapon %s" % p.path)

    prog("creating data partition on %s" % config.data.device.path)
    p = Partition(
        disk=disk_main,
        type=PARTITION_NORMAL,
        geometry=config.data
    )
    disk_main.addPartition(p, cnst_main)
    disk_main.commit()

    run("mkfs.ext4 %s" % p.path)
    run("mount %s /mnt" % p.path)
    mkdir("/mnt/boot")
    run("mount %s /mnt/boot" % config.efi)
