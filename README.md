# F5 Operating System (FFOS)

## Introduction
FFOS is a customised version of Arch Linux intended to streamline the process ofinstalling basic software to operate effectivley within our infomation systems. It is designed to provide a easy transition from Windows while also allowing low level access if required, whilst also being easier than windows by having automaticaly installed applications and utilities.

## Installation Procedure
As it is assumed that this is being installed as a dual boot OS with Windows 10, instructions will be given for preparing the installation on Wndows followed by finishing it on FFOS. If you are planning to triple boot or are already using Linux you may have issues which require manual intervention, but due to the fact that you are alreayd using Linux it is assumed that you know how to do this. In the case of any issue do not force anything, consult a member of the software team instead.

### Windows 10
#### Partition Disk
1. Launch disk management from the start menu
2. Select the drive you want to install on
3. Right Click >> Shrink Volume
4. Enter how much you space you want to allocate to FFOS, the minimum is 12GB, but at least 32GB is recomended
5. You should see a block on the button of the screen assoiated with the disk marked as unallocated
6. Exit partition manager

#### Create Live USB
1. Select an USB drive with no important data on it (the data will be permently erased)
2. Download the (latest) FFOS ISO from http://frc4739.freeddns.org/isos
3. Install ([Rufus](https://rufus.ie/))
4. Burn the image to the USB
    1. Insert the USB into the computer
    2. Open Rufus
    3. Select the USB as the device
    4. Select the ISO as the boot selection
    5. Set the partition scheme to GPT
    6. Click start (as the other options are ok)
    7. Set the image mode to DD instead of ISO

### Live USB
1. Boot from USB
    1. Press a assigned key to enter the BIOS (varies by manufacturer), it could be: 
        - Acer: `F2` or `DEL`
        - Asus: `F2`
        - Dell: `F2`
        - HP: `ESC` or `F10`
        - Lenovo: `Enter` then `F1`
        - MSI: `DEL`
        - Surface: Hold power button
        - Smasung: `F2`
        - Sony: `F1`, `F2` or `F3`
        - Toshiba: `F2`
        - Other: `F1`, `F2`, `F10`, `DEL`, `ESC`, `CTRL` + `ALT` + `ESC` or `CTRL` + `ALT` + `DEL`
    2. Disable secure boot
    3. Enter the boot menu and select the USB as the boot option
    4. Exit the BIOS
2. Follow the on screen instructions and prompts
    1. Navigate with the arrow keys and enter
    2. (De)select options with space
    3. Disable extra package
3. Reboot your system

## Usage Tips
### i3 WM
- Instead of a more common way to open and manage windows, FFOS uses i3
- It is a tiling window manager so windows don't overlap
- Applets offer shortcuts to applications which run in the background (like the system tray), they are located in the bottom right corner and include network settings
#### Important Hotkeys
- `Win` + `d`: open application launcher
- `Win` + `Shift` + `q`: close current application (the one your mouse is over)
- `Win` + `Enter`: open terminal
- `Win` + `[NUMBER]`: switch desktop (see bottom left of screen)
- `Win` + `Shift` + `[NUMBER]`: move application to desktop
- `Win` + `Shift` + `Arrow`: move application position
- `Win` + `f`: make current application full screen
- `Win` + `h`: open the next application to the right of the current one
- `Win` + `v`: open the next application bellow the current one
- [More](https://i3wm.org/docs/userguide.html)

### Installing Programs
- We don't install applications like Windows we use a package manager using the command line
- Search the [offical package database](https://www.archlinux.org/packages/) or the [user respository](https://aur.archlinux.org/) (anyone can submit there).
- Use the command `yay -Syu [PACKAGE]` to install things, giving the name of the package
- If you can't find the program download the Windows version and try to run it there is a chance that Wine will take care of it

### We use Arch BTW
- If you are having issues don't just search Linux, search for Arch Linux as you will get better results
- The [Arch Wiki](https://wiki.archlinux.org/) is a great resource

## Meathod of Operation
### Booting
1. GRUB boots off of a live usb
2. Arch Linux boots automatically as it is the only option
4. The bootstrap script is called

### Bootstrap script
1. Prompt the user to configure Wi-Fi if a wired connection is not found
2. Use pacman to install the full install script
3. Run install script ([install.sh](install/install.sh))

### Install script
1. Executre the prepare script ([prepare.sh](install/prepare.sh))
2. Chroot into the new partition running the final install script ([chroot_install.sh](install/chroot_install.sh))

### Prepare script
1. Print a disclaimer and liscence notice which must be excepted
2. Load infomation about the attached disk and partitions
3. Prompt the user for infomation about the install location
4. Create the partitions for Linux
5. Prompt the user for credentials
6. Retrieve additional user infomation from the team server based on the credentials
7. Format the new partitions
8. install the essential packages to the new install
9. Copy the next install script ([chroot_install.sh](install/chroot_install.sh)) to the new partition

### Final install script
1. Configure the account credentials
2. Set region settings
3. Install GRUB on the EFI partition of the device
4. Install the correct graphics drivers along with a fallback
5. Install Wi-Fi drivers if a Wi-Fi card is detected
6. Use pacman to install the correct set of packages off of the team server
7. Remove this script to keep things clean
8. Reboot the system
